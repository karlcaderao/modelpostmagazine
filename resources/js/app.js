require('./bootstrap');

import Vue from 'vue';
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';
import VCalendar from 'v-calendar';
import 'vue-event-calendar/dist/style.css' //^1.1.10, CSS has been extracted as one file, so you can easily update it.
import vueEventCalendar from 'vue-event-calendar'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Popover from 'vue-js-popover';

Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(VCalendar);
Vue.use(vueEventCalendar, {locale: 'en'});
Vue.use(ElementUI);
Vue.use(Popover)

Vue.component('customheader', require('./views/Header.vue').default);
Vue.component('magheader', require('./views/MagazineHeader.vue').default);
Vue.component('contentmanagemenu', require('./components/ContentManagementMenu.vue').default);
Vue.component('accountmanagemenu', require('./components/AccountManagementMenu.vue').default);
Vue.component('modelpostmenu', require('./components/ModelpostMagazineMenu.vue').default);
Vue.component('headersocial', require('./components/HeaderSocialLinks.vue').default);
Vue.component('topheader', require('./components/TopHeaderLinks.vue').default);
Vue.component('mypagesmenu', require('./components/MyPagesMenu.vue').default);
Vue.component('magtopheader', require('./components/MagazineTopHeader.vue').default);
Vue.component('magbanner', require('./components/MagazineBanner.vue').default);
Vue.component('magfreshposts', require('./components/MagazineFreshPosts.vue').default);
Vue.component('magfeatured', require('./components/MagazineFeatured.vue').default);
Vue.component('magtrending', require('./components/MagazineTrending.vue').default);
Vue.component('magmonthlyposts', require('./components/MagazineMonthlyPosts.vue').default);
Vue.component('magtopmembers', require('./components/MagazineTopMembers.vue').default);
Vue.component('magpopularposts', require('./components/MagazinePopularPosts.vue').default);
Vue.component('monthlyfeaturesection', require('./components/MonthlyFeatureSection.vue').default);
Vue.component('monthlyartist', require('./components/MonthlyArtist.vue').default);
Vue.component('nextmonthfeature', require('./components/NextMonthFeatureSection.vue').default);
Vue.component('honorablementions', require('./components/HonorableMentionsSection.vue').default);
Vue.component('runnerupssection', require('./components/RunnerUpsSection.vue').default);
Vue.component('weeklypostsection', require('./components/WeeklyPostSection.vue').default);
Vue.component('worthwatchingsection', require('./components/WorthWatchingSection.vue').default);
Vue.component('staffpickssection', require('./components/StaffPicksSection.vue').default);
Vue.component('messengerchats', require('./views/MessengerChats.vue').default);
Vue.component('messengersubchats', require('./views/MessengerSubChats.vue').default);
Vue.component('messengeruserchat', require('./components/MessengerUserChat.vue').default);
Vue.component('fanstab', require('./components/FansMessengerTab.vue').default);
Vue.component('memberstab', require('./components/MembersMessengerTab.vue').default);
Vue.component('createlistmodal', require('./components/CreateNewList.vue').default);
Vue.component('createmessagemodal', require('./components/CreateNewMessage.vue').default);
Vue.component('messengerwelcome', require('./components/MessengerWelcome.vue').default);
Vue.component('messengersubwelcome', require('./components/MessengerSubWelcome.vue').default);
Vue.component('messengercontent', require('./components/MessengerContent.vue').default);
Vue.component('cmsrightsidebar', require('./views/CmsRightSidebar.vue').default);
Vue.component('streamingsidebar', require('./components/StreamingSidebar.vue').default);
Vue.component('streamingcontent', require('./components/StreamingContent.vue').default);
Vue.component('cmsapptab', require('./components/CMSApplicationsTab.vue').default);
Vue.component('cmscontent', require('./components/CMSContent.vue').default);
Vue.component('cmssingleapp', require('./components/CMSSingleApplication.vue').default);
Vue.component('cmssinglereported', require('./components/CMSSingleReported.vue').default);
Vue.component('cmsdenyresponsemodal', require('./components/CMSDenyResponseModal.vue').default);
Vue.component('cmsresponsemodal', require('./components/CMSResponseDetailsModal.vue').default);
Vue.component('cmsreportedtab', require('./components/CMSReportedTab.vue').default);
Vue.component('cmssingleuser', require('./components/CMSSingleUser.vue').default);
Vue.component('cmsqueuedlist', require('./components/CMSQueuedList.vue').default);
Vue.component('cmseditorschoice', require('./components/CMSEditorsChoice.vue').default);
Vue.component('cmsmembersmonth', require('./components/CMSMembersMonth.vue').default);
Vue.component('cmsfreshpost', require('./components/CMSFreshlyPosted.vue').default);
Vue.component('cmsmonthlyfeature', require('./components/CMSMonthlyFeature.vue').default);

Vue.component('timeline', require('./components/Timeline.vue').default);
Vue.component('purchases', require('./components/Purchases.vue').default);
Vue.component('posts', require('./components/Posts.vue').default);
Vue.component('imagesets', require('./components/ImageSets.vue').default);
Vue.component('videos', require('./components/Videos.vue').default);
Vue.component('myschedule', require('./components/MySchedule.vue').default);
Vue.component('promotions', require('./components/Promotions.vue').default);
Vue.component('featured', require('./components/Featured.vue').default);
Vue.component('recommended', require('./components/Recommended.vue').default);
Vue.component('mobile_tabs', require('./components/MobileTabs.vue').default);
Vue.component('sub_mobile_tabs', require('./components/SubMobileTabs.vue').default);
Vue.component('bookmarked', require('./components/Bookmarked.vue').default);
Vue.component('messagepurchases', require('./components/MessagePurchases.vue').default);
Vue.component('privatevideos', require('./components/PrivateVideos.vue').default);
Vue.component('subscriberimagesets', require('./components/SubscriberImageSets.vue').default);
Vue.component('favoriteartists', require('./components/FavoriteArtists.vue').default);
Vue.component('mycalendar', require('./components/MyCalendar.vue').default);
Vue.component('trendingposts', require('./components/TrendingPosts.vue').default);
Vue.component('informationprvicacy', require('./components/InformationPrivacy.vue').default);
Vue.component('publicinfo', require('./components/PublicInfo.vue').default);
Vue.component('accountsettings', require('./components/AccountSettings.vue').default);
Vue.component('payaccount', require('./components/PayAccount.vue').default);
Vue.component('paymentcard', require('./components/PaymentCard.vue').default);
Vue.component('memberverification', require('./components/MemberVerification.vue').default);
Vue.component('notifmessaging', require('./components/NotificationsMessaging.vue').default);
Vue.component('settingtabs', require('./components/SettingMobileTabs.vue').default);
Vue.component('userprivacy', require('./components/UserPrivacy.vue').default);
Vue.component('userpublicinfo', require('./components/UserPublicInfo.vue').default);
Vue.component('usernotif', require('./components/UserNotificationsMessaging.vue').default);
Vue.component('usersettingtabs', require('./components/UserSettingTabs.vue').default);
Vue.component('streammemberstab', require('./components/StreamingMembersTab.vue').default);
Vue.component('paymentbalances', require('./components/PaymentBalances.vue').default);
Vue.component('saleshistory', require('./components/SalesHistory.vue').default);
Vue.component('paymenthistory', require('./components/PaymentHistory.vue').default);
Vue.component('paymentcongrats', require('./components/PaymentCongrats.vue').default);
Vue.component('saleschart', require('./components/SalesChart.vue').default);
Vue.component('comparisonchart', require('./components/ComparisonChart.vue').default);
Vue.component('subpaymenthistory', require('./components/SubPaymentHistory.vue').default);
Vue.component('subpurchasechart', require('./components/SubPurchaseChart.vue').default);
Vue.component('theartists', require('./components/TheArtists.vue').default);
Vue.component('myreferrals', require('./components/MyReferrals.vue').default);
Vue.component('following', require('./components/Following.vue').default);
Vue.component('castingtravelnotices', require('./components/CastingTravelNotices.vue').default);
Vue.component('streamtoptippers', require('./components/StreamingTopTippers.vue').default);
Vue.component('streamsetup', require('./components/StreamingStreamSetup.vue').default);
Vue.component('streamblockedusers', require('./components/StreamingBlockedUsers.vue').default);
Vue.component('streamprivatechat', require('./components/StreamingPrivateChat.vue').default);
Vue.component('streamlivestats', require('./components/StreamingLiveStats.vue').default);
Vue.component('streamchats', require('./components/StreamingChats.vue').default);
Vue.component('streamlivevideo', require('./components/StreamingLiveVideo.vue').default);
Vue.component('streamsubcontent', require('./components/StreamingSubscriberContent.vue').default);
Vue.component('streamsublivevideo', require('./components/StreamSubscriberVideo.vue').default);
Vue.component('streamsubtoptippers', require('./components/StreamingSubTopTippers.vue').default);
Vue.component('contentimagevideo', require('./components/ContentImageVideo.vue').default);
Vue.component('commentsection', require('./components/CommentSection.vue').default);
Vue.component('profilemember', require('./components/ProfileMember.vue').default);

let routes = [
    { path: '/messenger-subscriber', component: require('./components/MessengerSubWelcome.vue').default },
    { path: '/messenger-member', component: require('./components/MessengerWelcome.vue').default },
    { path: '/messenger-member/sample/list', component: require('./components/MessengerConversation.vue').default },
    { path: '/messenger-member/:sid/chat', component: require('./components/MessengerUserChat.vue').default },
    { path: '/messenger-subscriber/:sid/chat', component: require('./components/MessengerUserChat.vue').default },
    { path: '/messenger-member/list/all-files', component: require('./components/MessengerConversationFiles.vue').default },
    { path: '/cms/application', component: require('./components/CMSSingleApplication.vue').default },
    { path: '/cms/reported', component: require('./components/CMSSingleReported.vue').default },
    { path: '/cms/users/:id/details', component: require('./components/CMSSingleUser.vue').default },
    { path: '/cms/magazine/:id', name: 'magcontent', component: require('./components/CMSMagazineContent.vue').default },
];

let router = new VueRouter({
	base : 'modelpostmagazine',
	routes,
	mode: 'history'
});

const app = new Vue({
    el: '#app',
    router,
    vuetify: new Vuetify(),
    icons: {
        iconfont: 'fa4',
    },
  });