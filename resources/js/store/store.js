import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export default new Vuex.Store({
	state: {
        sectionID: []
	},
	getters: {
		getResponseData(state) {
			return state.sectionID;
		}
	},
	mutations: {
		setResponseData(state, payload) {
			state.sectionID = payload
		}
	},
	actions: {
		setResponseData(state, payload) {
			state.commit('setResponseData', payload)
		}
	}
});