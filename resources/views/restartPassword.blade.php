@extends('layouts.landing')
@section('content')
  <section id="reset-password">
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-12 py-0" style="height: 100vh;">
                <!-- <div class="mobile-logo">
                    <a href="#"><img class="logo" src="images/logo-transparent.png" alt="Model Post Magazine logo"></a>
                </div> -->
                <div class="mobile-overlay"></div>
                <div class="bg-image" style="background-image:url({{url('images/login-image-desktop.png')}})">
                    <div class="login-form">
                        <h3>Reset Password</h3>
                        <p>Please enter the email you used to register for your ModelPost Magazine account and you will be sent a password reset link in your email.
                        </p>
                        <form action="">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input type="email" placeholder="E-mail Address" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                            </div>
                            <button type="submit">Reset Password</button>
                        </form>
                        <div class="go-back-wrapper mt-2">
                            <a href="#">Go Back</a>
                        </div>
                    </div>
                </div>
                <div class="bg-color-inclined"></div>
                <div class="content-wrapper">
                    <a href="#"><h1 class="text-uppercase"><span class="prefix">Model</span><span class="middle">Post</span> <span class="suffix">Magazine</span></h1></a>
                    <p class="note">Staying connected with the Artists you love</p>
                    <ul class="checklist">
                        <li><i class="fas fa-check"></i><span>80% to 85% Commission goes back to Artist</span></li>
                        <li><i class="fas fa-check"></i><span>Commission on Artist Referrals</span></li>
                        <li><i class="fas fa-check"></i><span>Community Created Platform</span></li>
                    </ul>
                </div>
                <div class="bg-color-bottom">
                   <div class="social-links-wrapper">
                        <ul class="social-links">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                   </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection