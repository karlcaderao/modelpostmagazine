@extends('layouts.web')
@section('content')
  <section id="profile-banner" class="payment">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12 py-0 px-0">
                <div data-height="375" class="caption bottom-30 profile-banner-wrapper">
                    <div class="caption-overlay bg-gradient-fade"></div>
                    <img src="images/profilebanner.png" alt="" style="width:100%">
                    <!-- <div class="caption-background bg-0" style="background-image:url({{url('images/profilebanner.png')}})"></div> -->
                </div>
                
                <div class="fade-content">
                    <h1 class="text-left">My Payments</h1>
                    <hr class="custom-hr"> 
                    <div class="row">
                        <div class="col-md-8 payment-code-note">
                            <p>All your payment information in an easy to use layout</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section id="profile-member-content" class="paymentSub">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12">
                <div class="row row-content">
                    <div class="col-xl-8 col-lg-12">
                        <subpaymenthistory></subpaymenthistory>
                    </div>
                    <div class="col-xl-4 col-lg-12">
                        <subpurchasechart></subpurchasechart>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection