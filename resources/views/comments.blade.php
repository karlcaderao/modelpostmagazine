@extends('layouts.web')
@section('content')
  <section id="profile-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12 py-0 px-0">
                <div data-height="375" class="caption bottom-30 profile-banner-wrapper">
                    <div class="caption-overlay bg-gradient-fade"></div>
                    <img src="images/profilebanner.png" alt="" style="width:100%">
                    <!-- <div class="caption-background bg-0" style="background-image:url({{url('images/profilebanner.png')}})"></div> -->
                </div>
                <div class="profile-img-content">
                    <div class="profile-img-wrapper">
                        <img src="images/placeholder-image.jpg" alt="Profile Picture">
                        <div class="icon-wrapper d-none">
                            <i class="fas fa-check-circle"></i>
                        </div>
                    </div>
                    <a href="#" class="btn-follow">Follow</a>
                    <a href="#" class="btn-unfollow d-none">unfollow</a>
                </div>
                <div class="fade-content">
                    <h1 class="text-left text-uppercase">Adriana Lima</h1>
                    <hr class="custom-hr"> 
                    <div class="">
                        <div class="">
                            
                            <div class="action-buttons">
                                <div>
                                    <a href="#" class="color-highlight show-more mb-0">Show less <i class="fa fa-chevron-up"></i></a>
                                </div>
                                <!-- <div>
                                    <a href="#"><i class="far fa-star"></i>Follow</a>
                                    <a href="#"><i class="fas fa-video"></i>Stream Live</a>
                                    <a href="#"><i class="fas fa-plus"></i>Create New</a>
                                </div> -->
                            </div>
                            <div class="more-content show-content">
                                <div class="counters">
                                    <div>
                                        <h4>1130</h4>
                                        <p class="title">Posts</p>
                                    </div>
                                    <div>
                                        <h4>201,145</h4>
                                        <p class="title">Likes</p>
                                    </div>
                                    <div>
                                        <h4>450</h4>
                                        <p class="title">Followers</p>
                                    </div>
                                </div>
                                <div class="social-links">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                                <div class="note">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section id="mobile-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <mobile_tabs></mobile_tabs>
                </div>
            </div>
        </div>
  </section>
  <section id="profile-member-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12">
                <div class="row row-content">
                    <div class="col-xl-8 col-lg-12">
                        <contentimagevideo></contentimagevideo>
                        <commentsection></commentsection>
                    </div>
                    <div class="col-xl-4 col-lg-12">
                        <promotions></promotions>
                        <featured></featured>
                        <recommended></recommended>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection