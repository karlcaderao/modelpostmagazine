@extends('layouts.magazine')
@section('content')
    <section id="weekly-post-page">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs">
                        <a href="#" class="text-uppercase">model post magazine</a> <span>/</span> <a href="#" class="text-uppercase">the weekly post</a>
                    </div>
                </div>
                <div class="col-md-8">
                    <weeklypostsection></weeklypostsection>
                </div>
                <div class="col-md-4">
                    <worthwatchingsection></worthwatchingsection>
                    <staffpickssection></staffpickssection>
                </div>
            </div>
        </div>
    </section>
@endsection