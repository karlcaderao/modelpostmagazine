@extends('layouts.landing')
@section('content')
  <section id="register">
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-12 py-0 main-wrapper" style="height: 100vh;min-height:900px;">
                <div class="mobile-overlay"></div>
                <div class="bg-image" style="background-image:url({{url('images/login-image-desktop.png')}})">
                    <div class="login-form">
                        <h3>Create Account</h3>
                        <form action="">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" placeholder="Username" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                <div class="valid-wrapper d-none align-items-center">
                                    <div>
                                        <i class="fas fa-check-circle"></i>
                                    </div>
                                </div>
                                <div class="invalid-wrapper d-flex align-items-center">
                                    <div>
                                        <i class="fas fa-times-circle"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input type="email" placeholder="E-mail Address" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                <div class="valid-wrapper d-flex align-items-center">
                                    <div>
                                        <i class="fas fa-check-circle"></i>
                                    </div>
                                </div>
                                <div class="invalid-wrapper d-none align-items-center">
                                    <div>
                                        <i class="fas fa-times-circle"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i class="fas fa-lock"></i></span>
                                </div>
                                <input placeholder="Choose a Password" type="password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                <div class="valid-wrapper d-flex align-items-center">
                                    <div>
                                        <i class="fas fa-check-circle"></i>
                                    </div>
                                </div>
                                <div class="invalid-wrapper d-none align-items-center">
                                    <div>
                                        <i class="fas fa-times-circle"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i class="fas fa-lock"></i></span>
                                </div>
                                <input placeholder="Repeat Password" type="password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                <div class="valid-wrapper d-flex align-items-center">
                                    <div>
                                        <i class="fas fa-check-circle"></i>
                                    </div>
                                </div>
                                <div class="invalid-wrapper d-none align-items-center">
                                    <div>
                                        <i class="fas fa-times-circle"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i class="fas fa-user-check"></i></span>
                                </div>
                                <input placeholder="Referral Code" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                <div class="valid-wrapper d-flex align-items-center">
                                    <div>
                                        <i class="fas fa-check-circle"></i>
                                    </div>
                                </div>
                                <div class="invalid-wrapper d-none align-items-center">
                                    <div>
                                        <i class="fas fa-times-circle"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="form-note mb-2">Get an additional 5% in commissions for your first year when using a referral code</p>
                            <div class="checkbox-terms d-flex align-items-baseline">
                                <input type="checkbox" id="terms" name="terms" value="agree">
                                <label for="terms">By signing up you agree to our <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a></label><br>
                            </div>
                            <button type="submit">Create Account</button>
                        </form>
                        <div class="text-center mt-2">
                            <p class="mb-1">Already Registered? <a href="#" class="create-acct-link">Login in Here</a></p>
                        </div>
                        <div class="login-alternative d-flex flex-column text-center">
                            <a href="#" class="btn-twitter"><i class="fab fa-twitter"></i> Login With Twitter</a>
                            <a href="#" class="btn-facebook"><i class="fab fa-facebook-f"></i> Login With Facebook</a>
                        </div>
                    </div>
                </div>
                <div class="bg-color-inclined"></div>
                <div class="content-wrapper">
                    <a href="#"><h1 class="text-uppercase"><span class="prefix">Model</span><span class="middle">Post</span> <span class="suffix">Magazine</span></h1></a>
                    <div class="buttons d-flex justify-content-center">
                        <a href="#" class="features-link">Features <i class="fa fa-chevron-up"></i></a>
                        <a href="#" class="requirements-link">Requirements <i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="features hide-content d-none">
                        <ul class="checklist">
                            <li><i class="fas fa-check"></i><p>85% Commission when referred by another member*</p></li>
                            <li><i class="fas fa-check"></i><p>10% Commission on referrals for your first year*</p></li>
                            <li><i class="fas fa-check"></i><p>Pay-per-view Messaging & Tips</p></li>
                            <li><i class="fas fa-check"></i><p>Live Streaming</p></li>
                            <li><i class="fas fa-check"></i><p>Voice Calls</p></li>
                            <li><i class="fas fa-check"></i><p>Weekly Payouts</p></li>
                            <li><i class="fas fa-check"></i><p>DMCA Protection</p></li>
                            <li><i class="fas fa-check"></i><p>Always free to use</p></li>
                        </ul>
                        <div class="note">
                            <p class="title font-weight-bold">*Introductory Rates for Preferred Members during our first year!</p>
                            <p>85% Commission on sales during your introductory year. After the 
                            introductory rate has ended a standard 5% commission will
                            continue moving forward
                            </p>
                            <p>10% Referral Commission during your introductory year. After which
                            the rate of the a standard 5% Referral Commission will
                            continue moving forward
                            </p>
                        </div>
                    </div>
                    <div class="requirements d-none hide-content">
                        <ul class="checklist">
                            <li><i class="fas fa-check"></i><p>18 Years of age or older</p></li>
                            <li><i class="fas fa-check"></i><p>Industry Member *</p></li>
                            <li><i class="fas fa-check"></i><p>Fan Page or Social Media Account for verification **</p></li>
                        </ul>
                        <div class="note mt-5">
                            <p class="title font-weight-bold">* Industry Members include</p>
                            <p>Models (print, digital, social media content creators), Photographers, Designers
                            Film Makers, Hair and  Makeup Artists
                            </p>
                        </div>
                        <div class="note mt-5">
                            <p class="title font-weight-bold">** Member Verification</p>
                            <p>Verified Members will be contacted through their social media
                            fan page or website when confirming identify
                            </p>
                        </div>
                    </div>
                </div>
                <div class="bg-color-bottom">
                   <div class="social-links-wrapper">
                        <ul class="social-links">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                   </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection