@extends('layouts.magazine')
@section('content')
    <section id="monthly-feature-page">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs">
                        <a href="#" class="text-uppercase">model post magazine</a> <span>/</span> <a href="#" class="text-uppercase">Monthly feature</a>
                    </div>
                </div>
                <div class="col-md-8">
                    <monthlyfeaturesection></monthlyfeaturesection>
                    <nextmonthfeature></nextmonthfeature>
                    <honorablementions></honorablementions>
                </div>
                <div class="col-md-4">
                    <runnerupssection></runnerupssection>
                    <trendingposts></trendingposts>
                </div>
            </div>
        </div>
    </section>
@endsection