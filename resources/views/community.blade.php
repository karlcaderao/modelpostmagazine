@extends('layouts.web')
@section('content')
  <section id="profile-banner" class="community-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12 py-0 px-0">
                <div data-height="375" class="caption bottom-30 profile-banner-wrapper">
                    <div class="caption-overlay bg-gradient-fade"></div>
                    <img src="images/community-banner.jpg" alt="" style="width:100%">
                    <!-- <div class="caption-background bg-0" style="background-image:url({{url('images/profilebanner.png')}})"></div> -->
                </div>
                <div class="community-heading">
                    <div class="left-content">
                        <h1 class="text-left">Community & Castings</h1>
                        <p>Connecting and Creating made Safer and Easier</p>
                    </div>
                    <div class="right-content">
                        <h2 class="text-left text-uppercase">Artists</h2>
                        <div class="followers-wrapper">
                            <p class="text-uppercase">Following <span class="font-weight-bold">200</span></p>
                            <p class="text-uppercase"><span class="font-weight-bold">1,513</span> followers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section id="mobile-layout" class="community-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <mobile_tabs></mobile_tabs>
                </div>
            </div>
        </div>
  </section>
  <section id="profile-member-content" class="community-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12">
                <div class="row row-content">
                    <div class="col-xl-12 col-lg-12">
                        <myreferrals></myreferrals>
                        <following></following>
                        <castingtravelnotices></castingtravelnotices>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection