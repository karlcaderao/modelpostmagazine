@extends('layouts.web')
@section('content')
  <section id="setting-mobile-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <usersettingtabs></usersettingtabs>
                </div>
            </div>
        </div>
  </section>
  <section id="profile-member-content" class="settingMember">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-lg-10 offset-lg-2 col-md-12">
                <div class="row">
                    <div class="col-xl-8 col-lg-12 p-0">
                       <cmscontent></cmscontent>
                    </div>
                    <div class="col-xl-4 col-lg-12 chat-sidebar">
                        <cmsrightsidebar></cmsrightsidebar>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection