@extends('layouts.web')
@section('content')
  <section id="profile-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12 py-0">
                <div data-height="375" class="caption bottom-30 profile-banner-wrapper" style="height: 375px;">
                    <div class="caption-overlay bg-gradient-fade"></div>
                    <div class="caption-background bg-0" style="background-image:url({{url('images/profilebanner.png')}})"></div>
                </div>
                <div class="fade-content">
                    <h1 class="text-left text-uppercase">Welcome Back</h1>
                    <hr class="custom-hr"> 
                    <div class="">
                        <div class="">
                            
                            <div class="action-buttons">
                                <div>
                                    <a href="#" class="color-highlight show-more mb-0">Show less <i class="fa fa-chevron-up"></i></a>
                                </div>
                                <!-- <div>
                                    <a href="#"><i class="far fa-star"></i>Follow</a>
                                    <a href="#"><i class="fas fa-video"></i>Stream Live</a>
                                    <a href="#"><i class="fas fa-plus"></i>Create New</a>
                                </div> -->
                            </div>
                            <div class="more-content show-content">
                                <div class="note">
                                    <p>This page is a private collection of your Purchases, Bookmarks, and Favoite Artists.</p>
                                </div>
                                <div class="counters">
                                    <div>
                                        <h4>1130</h4>
                                        <p class="title">Artists Supported</p>
                                    </div>
                                    <div>
                                        <h4>201,145</h4>
                                        <p class="title">Likes</p>
                                    </div>
                                    <div>
                                        <h4>450</h4>
                                        <p class="title">Artists Following</p>
                                    </div>
                                </div>
                                <!-- <div class="social-links">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section id="sub-mobile-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <sub_mobile_tabs></sub_mobile_tabs>
                </div>
            </div>
        </div>
  </section>
  <section id="profile-subscriber-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12">
                <div class="row row-content">
                    <div class="col-xl-8 col-lg-12">
                        <timeline></timeline>
                        <favoriteartists></favoriteartists>
                        <bookmarked></bookmarked>
                        <messagepurchases></messagepurchases>
                        <subscriberimagesets></subscriberimagesets>
                        <privatevideos></privatevideos>
                        <mycalendar></mycalendar>
                    </div>
                    <div class="col-xl-4 col-lg-12">
                        <promotions></promotions>
                        <featured></featured>
                        <recommended></recommended>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection