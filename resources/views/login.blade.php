@extends('layouts.landing')
@section('content')
  <section id="login">
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-12 py-0 main-wrapper" style="height: 100vh;">
                <!-- <div class="mobile-logo">
                    <a href="#"><img class="logo" src="images/logo-transparent.png" alt="Model Post Magazine logo"></a>
                </div> -->
                <div class="mobile-overlay"></div>
                <div class="bg-image" style="background-image:url({{url('images/login-image-desktop.png')}})">
                    <div class="login-form">
                        <h3>Welcome back,</h3>
                        <form action="">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" placeholder="Username" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default"><i class="fas fa-lock"></i></span>
                                </div>
                                <input placeholder="Password" type="password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                            </div>
                            <button type="submit">Enter</button>
                            <div class="text-center mt-2 mb-3">
                                <a href="#">Forgot Password?</a>
                            </div>
                        </form>
                        <div class="text-center">
                            <p class="mb-1">Don't have an account yet? <a href="#" class="create-acct-link">Create your Account</a></p>
                        </div>
                        <div class="login-alternative d-flex flex-column text-center">
                            <a href="#" class="btn-finger-print">Finger Print</a>
                            <a href="#" class="btn-twitter"><i class="fab fa-twitter"></i> Login With Twitter</a>
                            <a href="#" class="btn-facebook"><i class="fab fa-facebook-f"></i> Login With Facebook</a>
                        </div>
                    </div>
                </div>
                <div class="bg-color-inclined"></div>
                <div class="content-wrapper">
                    <a href="#"><h1 class="text-uppercase"><span class="prefix">Model</span><span class="middle">Post</span> <span class="suffix">Magazine</span></h1></a>
                    <p class="note">Staying connected with the Artists you love</p>
                    <ul class="checklist">
                        <li><i class="fas fa-check"></i><span>80% to 85% Commission goes back to Artist</span></li>
                        <li><i class="fas fa-check"></i><span>Commission on Artist Referrals</span></li>
                        <li><i class="fas fa-check"></i><span>Community Created Platform</span></li>
                    </ul>
                </div>
                <div class="bg-color-bottom">
                   <div class="social-links-wrapper">
                        <ul class="social-links">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                   </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection