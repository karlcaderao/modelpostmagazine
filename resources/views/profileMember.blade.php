@extends('layouts.web')
@section('content')
  <section id="profile-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12 py-0 px-0">
                <profilemember></profilemember>
            </div>
        </div>
    </div>
  </section>
  <section id="mobile-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <mobile_tabs></mobile_tabs>
                </div>
            </div>
        </div>
  </section>
  <section id="profile-member-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12">
                <div class="row row-content">
                    <div class="col-xl-8 col-lg-12">
                        <timeline></timeline>
                        <purchases></purchases>
                        <posts></posts>
                        <imageSets></imageSets>
                        <videos></videos>
                        <mySchedule></mySchedule>
                    </div>
                    <div class="col-xl-4 col-lg-12">
                        <promotions></promotions>
                        <featured></featured>
                        <recommended></recommended>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection