<div id="bottom-sidebar">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="widget-area-14">
                    <div class="widget">
                        <div class="widget-content">
                            <a href="#"><img src="images/logo-transparent.png" alt="logo"></a>
                        </div>
                    </div>

                    <div class="widget widget_text">
                        <div class="textwidget">

                            <div class="kopa-text">
                                <p>Model Post Magazine is a free portfolio platform and printed magazine were artist can publish their work.
                                    Free of charge, free from usage limits, and free from intrusive censorship rules.</p>
                                <p> Model Post Magazine connects all of your social media networks together and grows your fan base.
                                    But most importantly Model Post Magazine helps support artist for the work they created.</p>
                            </div>
                        </div>
                    </div>
                    <!-- widget text -->
                    <div class="widget kopa-social-3-widget">
                        <div class="widget-content">
                            <div class="kopa-social clearfix">
                                <a href="#" class="fab fa-facebook-f"></a>
                                <a href="#" class="fab fa-twitter"></a>
                                <a href="#" class="fab fa-instagram"></a>
                                <a href="#" class="fab fa-youtube"></a>
                            </div>
                            <!-- social -->
                        </div>
                    </div>
                    <!-- <div class="widget kopa-newletter-widget">
                        <div class="widget-content">
                            <form action="/" method="post" class="form-news-letter">
                                <input type="text"  name="search-text" onBlur="if (this.value == '')
                                            this.value = this.defaultValue;" onFocus="if (this.value == this.defaultValue)
                                                        this.value = '';" value="Your email here..." >
                                <button type="submit" class="fa fa-envelope"></button>
                            </form>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- col 3 -->
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="widget-area-15">
                    <div class="widget kopa-single-post-widget">

                        <div class="widget-content">
                            <div class="item">
                                <div class="post-thumb">
                                    <a href="#" class="img-responsive"><img src="images/footer-img.jpg" alt=""></a>
                                </div>
                                <!-- post thumb -->
                                <div class="kopa-metadata">
                                    <span class="kopa-date"><i class="fas fa-calendar-alt"></i> April 20, 2016</span>
                                    <span class="kopa-rate">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                </div>
                                <!-- metadata -->
                                <div class="item-content">
                                    <h4 class="post-title text-uppercase"><a href="#">2016 Year in Review</a></h4>
                                    <div class="post-content">
                                        <p>On set with Model Post Magazine for the filming of the "Year in Review"...</p>
                                    </div>
                                </div>
                            </div>
                            <!-- item -->
                        </div>
                        <!-- widget-cotent -->
                    </div>
                    <!-- single post -->
                </div>
            </div>
            <!-- col 3 -->
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="widget-area-16">
                    <div class="widget kopa-tweet-widget">
                        <header class="widget-header">
                            <h3 class="widget-title text-uppercase">recent tweets</h3>
                            <i class="fa fa-plus-square-o"></i>
                        </header>
                        <div class="widget-content">
                            <div class="tweets" data-user="kopasoft">
                                <ul class="tweetList">
                                    <li><i class="fab fa-twitter"></i> <p>Lorem ipsum dolor sit amet, on sectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore...</p></li>
                                </ul>
                            </div>
                        </div>
                        <!-- widget-content -->

                    </div>
                    <!-- kp-tweet -->
                </div>
            </div>
            <!-- col 3 -->
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="widget-area-17">
                    <div class="widget kopa-flickr-widget">
                        <header class="widget-header">
                            <h3 class="widget-title text-uppercase">Instagram</h3>
                            <i class="fa fa-plus-square-o"></i>
                        </header>
                        <div class="widget-content">
                            <div class="flickr-wrap clearfix" data-user="78715597@N07">
                                <ul class="clearfix list-unstyled">
                                    <li><a href="#"><img src="images/placeholder-image.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/placeholder-image.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/placeholder-image.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/placeholder-image.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/placeholder-image.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/placeholder-image.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/placeholder-image.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/placeholder-image.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/placeholder-image.jpg" alt=""></a></li>
                                </ul>
                            </div>
                            <!--flickr-wrap-->
                        </div>
                        <!-- widget-content -->

                    </div>
                    <!-- kp-tweet -->
                </div>
            </div>
            <!-- col 3 -->
        </div>
        <!-- row -->
    </div>
    <!-- container -->
</div>
<!-- bottom sidebar -->
<footer id="kopa-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12 copy-right-wrapper">
                <div class="copy-right pull-left">
                    <p>2020 ModelPost Magazine - All Rights Reserved.</p>
                </div>
            </div>
            <!-- copy right -->
            <div class="col-md-6 col-12 menu-footer-wrapper">
                <div class="menu-third pull-right">
                    <ul>
                        <li><a href="index.php">Site Guidelines</a></li>
                        <li><a href="Model_List.php">Member Guidelines</a></li>
                        <li><a href="Join.php">Support</a></li> 
                    </ul>
                </div>
            </div>
        </div>
    </div>    <!-- container -->
</footer><!-- page footer -->
<script type="text/javascript" src="{{ url('js/app.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('js/gistfile1.js') }}"></script>
<script type="text/javascript" src="{{ url('js/custom.js') }}"></script>
<script>
    window.baseUrl = "{{ url('/') }}";
</script>