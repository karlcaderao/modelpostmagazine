<div class="container-fluid setting">
    <div class="row">
        <div class="col-lg-2 col-md-3" id="sidebar" data-menu-load="includes/nav/menuUser.php" data-menu-width="250" data-menu-effect="menu-parallax">
            <div class="sidebar">
                <header>
                    <div class="logo-wrapper">
                        <a href="#" class="mobile-close"><i class="fas fa-times"></i></a>
                        <a href="#" class="dark"><img src="images/logo-transparent-dark.png" alt="logo"></a>
                        <a href="#" class="light"><img src="images/logo-transparent.png" alt="logo"></a>
                    </div>
                    <div class="pages-menu">
                        <p class="text-muted text-uppercase">My pages</p>
                        <ul>
                            <li><a href="#"><i class="fas fa-user"></i> Profile</a></li>
                            <li><a href="#"><i class="fas fa-money-bill"></i> Sales</a></li>
                            <li><a href="#"><i class="fas fa-envelope"></i> Messenger</a></li>
                            <li><a href="#"><i class="fas fa-video"></i> stream live</a></li>
                            <li><a href="#"><i class="fas fa-plus"></i> create new content</a></li>
                            <li><a href="#"><i class="fas fa-users"></i> community & castings</a></li>
                            <li><a href="#"><i class="fas fa-bell"></i> notifications</a></li>
                            <li><a href="#"><i class="fas fa-cog"></i> settings</a></li>
                        </ul>
                    </div>
                    <div class="pages-menu">
                        <p class="text-muted text-uppercase">Modelpost magazine</p>
                        <ul>
                            <li><a href="#"><i class="fas fa-images"></i> Magazine home</a></li>
                            <li><a href="#"><i class="fas fa-newspaper"></i> The weekly Post</a></li>
                            <li><a href="#"><i class="fas fa-bullhorn"></i> monthly feature</a></li>
                            <li><a href="#"><i class="fas fa-list"></i> member list</a></li>
                            <li><a href="#"><i class="fas fa-sign-out-alt" style="position:relative;"> Logout</i></a></li>
                        </ul>
                    </div>

                    <div class="social-menu">
                        <p class="text-muted text-uppercase">Social Links</p>
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </header>
            </div>
        </div>
        <div class="col-lg-10 col-md-12" id="top-header">
            <div class="top-header-wrapper">
                <div class="left-btn-wrapper">
                    <a href="#"><i class="fas fa-angle-left"></i>Back</a>
                </div>
                <div class="right-btn-wrapper">
                    <a href="#" class="header-icon header-icon-4" title="profile"><i class="fas fa-user"></i></a>
                    <a href="#" class="header-icon header-icon-4" title="acc content"><i class="fas fa-plus-circle"></i></a>
                    <a href="#" class="header-icon header-icon-4" title="messenger"><i class="fab fa-facebook-messenger"></i></a>
                    <a href="#" class="header-icon header-icon-4" title="stream live"><i class="fas fa-stream"></i></a>
                    <a href="#" class="header-icon header-icon-4" title="settings"><i class="fas fa-cog"></i></a>
                    <a href="#" class="header-icon header-icon-4 mode" title="day/night mode" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fas fa-moon"></i></a>
                    <a href="notifications.php" class="header-icon header-icon-3 bell" title="notifications"><i class="fa fa-bell fa color-red2-dark"></i></a>
                    <a href="javascript:void(0)" class="header-icon header-icon-2 hamburger-menu" data-menu="sidebar"><i class="fas fa-bars"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered">
    <div class="modal-content">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <div>
                        <h3 class="ultrabold font-22 bottom-0 top-25">Day or Night</h3>
                        <p class="under-heading color-highlight font-11 bottom-0 text-lowercase">Choose the mode you Love</p>
                    </div>
                </div>
                <div class="col-4 d-flex align-items-center justify-content-center">
                    <div>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                            <i class="lightbulb fas fa-lightbulb"></i>
                            <i class="moon fas fa-moon"></i>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>