<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Model Post Magazine</title>
    <link rel="stylesheet" href="{{ url('/fonts/fontawesome/css/all.css') }}" type="text/css" media="screen">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" type="text/css" media="screen">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" type="text/css" media="screen">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ url('/css/app.css') }}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{ url('/css/frontend.css') }}" type="text/css" media="screen">
</head>
<body>
<button id="floatingTop" class="back-to-top" title="Go to top"><i class="fas fa-angle-up"></i></button>
    <div id="app">
        @include('layouts.magazineHeader')
        <div id="content">
            @yield('content')
        </div>
    </div>
            

    @include('layouts.magazineFooter')
</body>
</html>
