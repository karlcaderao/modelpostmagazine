<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Model Post Magazine</title>
    <link rel="stylesheet" href="{{ url('/css/app.css') }}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{ url('/css/frontend.css') }}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{ url('/fonts/fontawesome/css/all.css') }}" type="text/css" media="screen">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet"> 
</head>
<body>
    <div id="app">
        <div id="content">
            @yield('content')
        </div>
    </div>
</body>
<script type="text/javascript" src="{{ url('js/app.js') }}"></script>
<script type="text/javascript" src="{{ url('js/custom.js') }}"></script>
</html>
