<customheader></customheader>
<!-- Theme Mode Modal -->
<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered">
    <div class="modal-content">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <div>
                        <h3 class="ultrabold font-22 bottom-0 top-25">Day or Night</h3>
                        <p class="under-heading color-highlight font-11 bottom-0 text-lowercase">Choose the mode you Love</p>
                    </div>
                </div>
                <div class="col-4 d-flex align-items-center justify-content-center">
                    <div>
                        <label class="switch">
                            <input type="checkbox" checked>
                            <span class="slider round"></span>
                            <i class="lightbulb fas fa-lightbulb"></i>
                            <i class="moon fas fa-moon"></i>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<createlistmodal></createlistmodal>
<createmessagemodal></createmessagemodal>
<cmsdenyresponsemodal></cmsdenyresponsemodal>
<cmsresponsemodal></cmsresponsemodal>