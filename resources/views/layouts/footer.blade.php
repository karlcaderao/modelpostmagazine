<footer id="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12 text-center footer-wrapper">
               <div class="footer-top">
                    <h5 class="title">A Community Platform Build With <i class="fas fa-heart"></i></h5>
                    <h6 class="description">By Members, For Members</h6>
               </div>
               <div class="footer-social">
                    <ul>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="javascript:void(0)" class="back-to-top"><i class="fas fa-angle-up"></i></a></li>
                    </ul>
               </div>
               <hr>
               <div class="footer-copyright">
                  <p><span>&copy;</span> Copyright Enabled <?php echo date('Y'); ?>. All Rights Reserved.</p>
               </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="{{ url('js/app.js') }}"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg==" crossorigin="anonymous"></script>
<script src="//media.twiliocdn.com/sdk/js/video/releases/2.3.0/twilio-video.min.js"></script>
<script type="text/javascript" src="{{ url('js/custom.js') }}"></script>
<script>
    window.baseUrl = "{{ url('/') }}";
</script>