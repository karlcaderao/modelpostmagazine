@extends('layouts.web')
@section('content')
  <section id="setting-mobile-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <streammemberstab></streammemberstab>
                </div>
            </div>
        </div>
  </section>
  <section id="profile-member-content" class="streaming-page">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-lg-10 offset-lg-2 col-md-12">
                <div class="row">
                    <div class="col-xl-9 col-lg-12 p-0">
                       <streamingcontent></streamingcontent>
                    </div>
                    <div class="col-xl-3 col-lg-12 streaming-chat-sidebar">
                        <streamingsidebar></streamingsidebar>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection