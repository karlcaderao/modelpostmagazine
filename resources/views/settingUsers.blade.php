@extends('layouts.web')
@section('content')
  <section id="setting-mobile-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <usersettingtabs></usersettingtabs>
                </div>
            </div>
        </div>
  </section>
  <section id="profile-member-content" class="settingMember">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-2 col-md-12">
                <div class="row row-content settingMember">
                    <div class="col-12">
                        <h1 class="page-title text-uppercase">My settings</h1>
                    </div>
                    <div class="col-xl-8 col-lg-12">
                        <userprivacy></userprivacy>
                        <userpublicinfo></userpublicinfo>
                        <accountsettings></accountsettings>
                        <paymentcard></paymentcard>
                    </div>
                    <div class="col-xl-4 col-lg-12">
                        <usernotif></usernotif>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection