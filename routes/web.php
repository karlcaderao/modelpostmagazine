<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/{all}', function () {
//     return view('register');
// })->where(['all' => '.*']);

Route::get('/', function () {
    return view('profileMember');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/reset-password', function () {
    return view('restartPassword');
});


Route::get('/profile-member', function () {
    return view('profileMember');
});

Route::get('/profile-subscriber', function () {
    return view('profileSubscriber');
});

Route::get('/magazine', function () {
    return view('magazineHome');
});

Route::get('/monthly-feature', function () {
    return view('monthlyFeature');
});

Route::get('/weekly-post', function () {
    return view('weeklyPost');
});

Route::get('/members', function () {
    return view('members');
});

Route::get('/community', function () {
    return view('community');
});

Route::get('/setting-member', function () {
    return view('settingMembers');
});

Route::get('/setting-subscriber', function () {
    return view('settingUsers');
});

Route::get('/streaming-member', function () {
    return view('streaming');
});

// MEMBER API ROUTES
Route::group(['prefix' => 'api'], function () {
    Route::post('/grant-video', 'Member\StreamingController@grantVideo')->name('grant-video');
    Route::post('/compositions-video', 'Member\StreamingController@compositionsVideo')->name('compositions-video');
});

Route::get('/streaming-subscriber', function () {
    return view('streamingSubscriber');
});

Route::get('/messenger-member', function () {
    return view('messengerMember');
});

Route::get('/messenger-subscriber', function () {
    return view('messengerSubscriber');
});

Route::get('/payment-member', function () {
    return view('paymentMember');
});

Route::get('/payment-subscriber', function () {
    return view('paymentSubscriber');
});

Route::get('/cms', function () {
    return view('cms');
});

Route::get('/cms/users', function () {
    return view('cms');
});

Route::get('/cms/magazine', function () {
    return view('cms');
});

Route::get('/comments', function () {
    return view('comments');
});
