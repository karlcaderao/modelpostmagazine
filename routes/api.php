<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get-conversations', 'ConversationController@getAllConversations');


Route::post('/create-conversations', 'ConversationController@createConversation');
Route::post('/add-conversation-user', 'ConversationController@addUserToConversation');
Route::post('/get-participants', 'ConversationController@getParticipants');
Route::post('/get-messages', 'ConversationController@getConversationMessages');
Route::post('/send-message', 'ConversationController@sendMessage');
Route::post('/delete-message', 'ConversationController@deleteMessage');
Route::post('/edit-message', 'ConversationController@editMessage');
