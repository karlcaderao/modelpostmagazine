<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

class StreamingController extends Controller
{
	private $twilioAccountSid = 'AC97846085015ea5bd50260576546f2ffd';
	private $twilioApiKey = 'SKf875dd9ea6599985a5c14810f5c4ddba';
	private $twilioApiSecret = 'gewUwLFaRTw7GFcUSEa6A9YqrsBudrqk';

	public function grantVideo(Request $request) {
		// A unique identifier for this user
		$identity = $request->username;
		// The specific Room we'll allow the user to access
		$roomName = $request->room_name;  // -> Test: Modeling 101

		// Create access token, which we will serialize and send to the client
		$token = new AccessToken($this->twilioAccountSid, $this->twilioApiKey, $this->twilioApiSecret, 3600, $identity);

		// Create Video grant
		$videoGrant = new VideoGrant();
		$videoGrant->setRoom($roomName);

		// Add grant to token
		$token->addGrant($videoGrant);
		// render token to string
		return response()->json($token->toJWT());
	}

	public function compositionsVideo(Request $request) {
	}
}
