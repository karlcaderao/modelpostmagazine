<?php

namespace App\Http\Controllers;
// require_once '../vendor/autoload.php';

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\Rest\Client;


class ConversationController extends Controller
{
    
    public $sid;
    public $token;
    public $twilio;

    public function __construct()
    {
        $this->sid = "AC97846085015ea5bd50260576546f2ffd";
        $this->token = "1740a08302c67ef068d051308e5d358c";
        $this->twilio = new Client($this->sid, $this->token);
    }

    public function createConversation(Request $request){
        $data = $request->all();
        $conversation = $this->twilio->conversations->v1->conversations
                            ->create([
                                        "friendlyName" => $data['name']
                                    ]
                            );

        return $conversation->sid;
    }

    public function addUserToConversation(Request $request){
        $data = $request->all();
        $participant = $this->twilio->conversations->v1->conversations($data['cid'])
            ->participants
            ->create([
                        "identity" => $data['identity']
                    ]
            );

        return $participant->sid;
    }

    public function getParticipants(Request $request){
        $data = $request->all();
        $participants = $this->twilio->conversations->v1->conversations($data['cid'])
                                          ->participants
                                          ->read(20);
        $results = array();
        foreach ($participants as $record) {
            $data = array();
            $data['sid'] = $record->sid;
            $data['conversationSid'] = $record->conversationSid;
            $data['identity'] = $record->identity;
            $data['messagingBinding'] = $record->messagingBinding;
            $data['roleSid'] = $record->roleSid;
            $data['attributes'] = $record->attributes;
            $data['lastReadMessageIndex'] = $record->lastReadMessageIndex;
            $data['lastReadTimestamp'] = $record->lastReadTimestamp;
            $data['dateCreated'] = $record->dateCreated;
            $data['dateUpdated'] = $record->dateUpdated;
            $data['url'] = $record->url;
            array_push($results, $data);
        }
        return $results;
    }

    public function getConversationMessages(Request $request){
        $data = $request->all();
        $messages = $this->twilio->conversations->v1->conversations($data['cid'])
                ->messages
                ->read(20);
        $results = array();
        foreach ($messages as $record) {
            $temp = array();
            $temp['accountSid'] = $record->accountSid;
            $temp['conversationSid'] = $record->conversationSid;
            $temp['sid'] = $record->sid;
            $temp['index'] = $record->index;
            $temp['author'] = $record->author;
            $temp['body'] = $record->body;
            $temp['media'] = $record->media;
            $temp['attributes'] = $record->attributes;
            $temp['participantSid'] = $record->participantSid;
            $temp['dateCreated'] = $record->dateCreated;
            $temp['dateUpdated'] = $record->dateUpdated;
            $temp['url'] = $record->url;
            $temp['links'] = $record->links;
            array_push($results, $temp);
        }
        return $results;
    }

    public function sendMessage(Request $request){
        $data = $request->all();
        $message = $this->twilio->conversations->v1->conversations($data['cid'])
                                     ->messages
                                     ->create([
                                                  "author" => $data['username'],
                                                  "body" => $data['message'],
                                                  "participantSid" => "MBc0e848126a52459294a2d55071c7684c",
                                                //   "participantSid" => "MB1e7f5daaf7f847edbd9d3a9e22411f33",
                                              ]
                                     );

        return $message;
    }

    public function deleteMessage(Request $request){
        $data = $request->all();
        $this->twilio->conversations->v1->conversations($data['cid'])
                          ->messages($data['mid'])
                          ->delete();
    }

    public function editMessage(Request $request){
        $data = $request->all();
        $message = $this->twilio->conversations->v1->conversations($data['cid'])
                    ->messages($data['mid'])
                    ->update([
                                "body" => $data['message']
                            ]
                    );

        return $message->conversationSid;
    }

    public function getConversation($sid){
        $conversation = $this->twilio->conversations->v1->conversations($sid)
        ->fetch();

        print($conversation->chatServiceSid);
    }
    
    public function getAllConversations(){
        $conversations = $this->twilio->conversations->v1->conversations
                                           ->read(20);
        $results = array();
        foreach ($conversations as $record) {
            $data = array();
            $data['sid'] = $record->sid;
            $data['chatServiceSid'] = $record->chatServiceSid;
            $data['messagingServiceSid'] = $record->messagingServiceSid;
            $data['friendlyName'] = $record->friendlyName;
            $data['uniqueName'] = $record->uniqueName;
            $data['attributes'] = $record->attributes;
            $data['state'] = $record->state;
            $data['dateCreated'] = $record->dateCreated;
            $data['dateUpdated'] = $record->dateUpdated;
            $data['timers'] = $record->timers;
            $data['url'] = $record->url;
            $data['links'] = $record->links;
            array_push($results, $data);
        }
        return $results;
    }

    public function deleteConversation($cid){
        $this->twilio->conversations->v1->conversations($cid)
                          ->delete();
    }
}