/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/gistfile1.js":
/*!***********************************!*\
  !*** ./resources/js/gistfile1.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
* author Remy Sharp
* url http://remysharp.com/tag/marquee
*/
(function ($) {
  $.fn.marquee = function (klass) {
    var newMarquee = [],
        last = this.length; // works out the left or right hand reset position, based on scroll
    // behavior, current direction and new direction

    function getReset(newDir, marqueeRedux, marqueeState) {
      var behavior = marqueeState.behavior,
          width = marqueeState.width,
          dir = marqueeState.dir;
      var r = 0;

      if (behavior == 'alternate') {
        r = newDir == 1 ? marqueeRedux[marqueeState.widthAxis] - width * 2 : width;
      } else if (behavior == 'slide') {
        if (newDir == -1) {
          r = dir == -1 ? marqueeRedux[marqueeState.widthAxis] : width;
        } else {
          r = dir == -1 ? marqueeRedux[marqueeState.widthAxis] - width * 2 : 0;
        }
      } else {
        r = newDir == -1 ? marqueeRedux[marqueeState.widthAxis] : 0;
      }

      return r;
    } // single "thread" animation


    function animateMarquee() {
      var i = newMarquee.length,
          marqueeRedux = null,
          $marqueeRedux = null,
          marqueeState = {},
          newMarqueeList = [],
          hitedge = false;

      while (i--) {
        marqueeRedux = newMarquee[i];
        $marqueeRedux = $(marqueeRedux);
        marqueeState = $marqueeRedux.data('marqueeState');

        if ($marqueeRedux.data('paused') !== true) {
          // TODO read scrollamount, dir, behavior, loops and last from data
          marqueeRedux[marqueeState.axis] += marqueeState.scrollamount * marqueeState.dir; // only true if it's hit the end

          hitedge = marqueeState.dir == -1 ? marqueeRedux[marqueeState.axis] <= getReset(marqueeState.dir * -1, marqueeRedux, marqueeState) : marqueeRedux[marqueeState.axis] >= getReset(marqueeState.dir * -1, marqueeRedux, marqueeState);

          if (marqueeState.behavior == 'scroll' && marqueeState.last == marqueeRedux[marqueeState.axis] || marqueeState.behavior == 'alternate' && hitedge && marqueeState.last != -1 || marqueeState.behavior == 'slide' && hitedge && marqueeState.last != -1) {
            if (marqueeState.behavior == 'alternate') {
              marqueeState.dir *= -1; // flip
            }

            marqueeState.last = -1;
            $marqueeRedux.trigger('stop');
            marqueeState.loops--;

            if (marqueeState.loops === 0) {
              if (marqueeState.behavior != 'slide') {
                marqueeRedux[marqueeState.axis] = getReset(marqueeState.dir, marqueeRedux, marqueeState);
              } else {
                // corrects the position
                marqueeRedux[marqueeState.axis] = getReset(marqueeState.dir * -1, marqueeRedux, marqueeState);
              }

              $marqueeRedux.trigger('end');
            } else {
              // keep this marquee going
              newMarqueeList.push(marqueeRedux);
              $marqueeRedux.trigger('start');
              marqueeRedux[marqueeState.axis] = getReset(marqueeState.dir, marqueeRedux, marqueeState);
            }
          } else {
            newMarqueeList.push(marqueeRedux);
          }

          marqueeState.last = marqueeRedux[marqueeState.axis]; // store updated state only if we ran an animation

          $marqueeRedux.data('marqueeState', marqueeState);
        } else {
          // even though it's paused, keep it in the list
          newMarqueeList.push(marqueeRedux);
        }
      }

      newMarquee = newMarqueeList;

      if (newMarquee.length) {
        setTimeout(animateMarquee, 25);
      }
    } // TODO consider whether using .html() in the wrapping process could lead to loosing predefined events...


    this.each(function (i) {
      var $marquee = $(this),
          width = $marquee.attr('width') || $marquee.width(),
          height = $marquee.attr('height') || $marquee.height(),
          $marqueeRedux = $marquee.after('<div ' + (klass ? 'class="' + klass + '" ' : '') + 'style="display: block-inline; width: ' + width + 'px; height: ' + height + 'px; overflow: hidden;"><div style="float: left; white-space: nowrap;">' + $marquee.html() + '</div></div>').next(),
          marqueeRedux = $marqueeRedux.get(0),
          hitedge = 0,
          direction = ($marquee.attr('direction') || 'left').toLowerCase(),
          marqueeState = {
        dir: /down|right/.test(direction) ? -1 : 1,
        axis: /left|right/.test(direction) ? 'scrollLeft' : 'scrollTop',
        widthAxis: /left|right/.test(direction) ? 'scrollWidth' : 'scrollHeight',
        last: -1,
        loops: $marquee.attr('loop') || -1,
        scrollamount: $marquee.attr('scrollamount') || this.scrollAmount || 2,
        behavior: ($marquee.attr('behavior') || 'scroll').toLowerCase(),
        width: /left|right/.test(direction) ? width : height
      }; // corrects a bug in Firefox - the default loops for slide is -1

      if ($marquee.attr('loop') == -1 && marqueeState.behavior == 'slide') {
        marqueeState.loops = 1;
      }

      $marquee.remove(); // add padding

      if (/left|right/.test(direction)) {
        $marqueeRedux.find('> div').css('padding', '0 ' + width + 'px');
      } else {
        $marqueeRedux.find('> div').css('padding', height + 'px 0');
      } // events


      $marqueeRedux.bind('stop', function () {
        $marqueeRedux.data('paused', true);
      }).bind('pause', function () {
        $marqueeRedux.data('paused', true);
      }).bind('start', function () {
        $marqueeRedux.data('paused', false);
      }).bind('unpause', function () {
        $marqueeRedux.data('paused', false);
      }).data('marqueeState', marqueeState); // finally: store the state
      // todo - rerender event allowing us to do an ajax hit and redraw the marquee

      newMarquee.push(marqueeRedux);
      marqueeRedux[marqueeState.axis] = getReset(marqueeState.dir, marqueeRedux, marqueeState);
      $marqueeRedux.trigger('start'); // on the very last marquee, trigger the animation

      if (i + 1 == last) {
        animateMarquee();
      }
    });
    return $(newMarquee);
  };
})(jQuery);

/***/ }),

/***/ 2:
/*!*****************************************!*\
  !*** multi ./resources/js/gistfile1.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\laragon\www\modelpostmagazine\resources\js\gistfile1.js */"./resources/js/gistfile1.js");


/***/ })

/******/ });