/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/custom.js":
/*!********************************!*\
  !*** ./resources/js/custom.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function ($) {
  function goToTop(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 700);
  }

  $(document).ready(function () {
    //floating back to top button
    var mybutton = document.getElementById("floatingTop");

    window.onscroll = function () {
      scrollFunction();
    };

    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "flex";
      } else {
        mybutton.style.display = "none";
      }
    }

    $(".back-to-top").click(function () {
      $('html, body').animate({
        scrollTop: 0
      }, 700);
    });

    function goToTop() {
      $('html, body').animate({
        scrollTop: 0
      }, 700);
    } //register page features


    var windowWidth = $(window).width();

    if (windowWidth > 1199) {
      $("#register .content-wrapper .features").removeClass("d-none");
    } //register page switching content


    $("#register .content-wrapper .buttons a").click(function (e) {
      e.preventDefault();
      var windowWidth = $(window).width();
      var target = $(this).attr("class");
      $("#register .content-wrapper .buttons a").removeClass('active');
      var self = this;

      if (windowWidth <= 1199) {
        if (target == "features-link") {
          if ($("#register .content-wrapper .features").hasClass("hide-content")) {
            $("#register .content-wrapper .features").removeClass('hide-content');
            $("#register .content-wrapper .features").addClass('show-content');
            $("#register .content-wrapper .features").removeClass("d-none");
            $(self).html("Features <i class='fa fa-chevron-down'></i>");

            if (windowWidth <= 1199) {
              if ($("#register .content-wrapper .requirements").hasClass("hide-content")) {
                $("#register .main-wrapper").css("height", "1600px");
                $("#register .bg-image").css("height", "1600px");
                $("#register .login-form").css("top", "25%");
              } else {
                $("#register .main-wrapper").css("height", "2000px");
                $("#register .bg-image").css("height", "2000px");
                $("#register .login-form").css("top", "32%");
              }
            }
          } else {
            $("#register .content-wrapper .features").removeClass('show-content');
            $("#register .content-wrapper .features").addClass('hide-content');
            $("#register .content-wrapper .features").addClass("d-none");
            $(self).html("Features <i class='fa fa-chevron-up'></i>");

            if (windowWidth <= 1199) {
              if ($("#register .content-wrapper .requirements").hasClass("hide-content")) {
                $("#register .main-wrapper").css("height", "900px");
                $("#register .bg-image").css("height", "900px");
                $("#register .login-form").css("top", "7%");
              } else {
                $("#register .main-wrapper").css("height", "1350px");
                $("#register .bg-image").css("height", "1350px");
                $("#register .login-form").css("top", "20%");
              }
            }
          }
        } else {
          if ($("#register .content-wrapper .requirements").hasClass("hide-content")) {
            $("#register .content-wrapper .requirements").removeClass('hide-content');
            $("#register .content-wrapper .requirements").addClass('show-content');
            $("#register .content-wrapper .requirements").removeClass("d-none");
            $(self).html("Requirements <i class='fa fa-chevron-down'></i>");

            if (windowWidth <= 1199) {
              if ($("#register .content-wrapper .features").hasClass("hide-content")) {
                $("#register .main-wrapper").css("height", "1350px");
                $("#register .bg-image").css("height", "1350px");
                $("#register .login-form").css("top", "20%");
              } else {
                $("#register .main-wrapper").css("height", "2000px");
                $("#register .bg-image").css("height", "2000px");
                $("#register .login-form").css("top", "32%");
              }
            }
          } else {
            $("#register .content-wrapper .requirements").removeClass('show-content');
            $("#register .content-wrapper .requirements").addClass('hide-content');
            $("#register .content-wrapper .requirements").addClass("d-none");
            $(self).html("Requirements <i class='fa fa-chevron-up'></i>");

            if (windowWidth <= 1199) {
              if ($("#register .content-wrapper .features").hasClass("hide-content")) {
                $("#register .main-wrapper").css("height", "900px");
                $("#register .bg-image").css("height", "900px");
                $("#register .login-form").css("top", "7%");
              } else {
                $("#register .main-wrapper").css("height", "1600px");
                $("#register .bg-image").css("height", "1600px");
                $("#register .login-form").css("top", "25%");
              }
            }
          }
        }
      } else {
        if (target == "features-link") {
          $("#register .content-wrapper .requirements").hide();
          $("#register .content-wrapper .features").show();
        } else {
          $("#register .content-wrapper .features").hide();
          $("#register .content-wrapper .requirements").show();
          $("#register .content-wrapper .requirements").removeClass("d-none");
        }
      }
    }); //mobile-menu

    $(".hamburger-menu").on('click', function (event) {
      event.stopPropagation();
      $(this).removeClass('menu-active');
      $(this).addClass('menu-active');
      var menuData = $(this).data('menu');
      var menuID = $('#' + menuData);
      var menuEffect = $('#' + menuData).data('menu-effect');
      var menuWidth = menuID.data('menu-width');
      var menuHeight = menuID.data('menu-height');

      if (menuID.hasClass('menu-header-clear')) {
        menuHider.addClass('menu-active-clear');
      }

      function menuActivate() {
        menuID = 'menu-active' ? menuID.addClass('menu-active') : undefined;
      }

      if (menuID.hasClass('menu-box-left')) {
        $('#footer-menu').addClass('footer-menu-hidden');
      }

      if (menuID.hasClass('menu-box-bottom')) {
        $('#footer-menu').addClass('footer-menu-hidden');
      }

      if (menuID.hasClass('menu-box-right')) {
        $('#footer-menu').addClass('footer-menu-hidden');
      }

      if (menuEffect === "menu-parallax") {
        if (menuID.hasClass('menu-box-bottom')) {
          headerAndContent.css("transform", "translateY(" + menuHeight / 5 * -1 + "px)");
        }

        if (menuID.hasClass('menu-box-top')) {
          headerAndContent.css("transform", "translateY(" + menuHeight / 5 + "px)");
        }

        if (menuID.hasClass('menu-box-left')) {
          headerAndContent.css("transform", "translateX(" + menuWidth / 5 + "px)");
        }

        if (menuID.hasClass('menu-box-right')) {
          headerAndContent.css("transform", "translateX(" + menuWidth / 5 * -1 + "px)");
        }
      }

      if (menuEffect === "menu-push") {
        if (menuID.hasClass('menu-box-bottom')) {
          headerAndContent.css("transform", "translateY(" + menuHeight * -1 + "px)");
        }

        if (menuID.hasClass('menu-box-top')) {
          headerAndContent.css("transform", "translateY(" + menuHeight + "px)");
        }

        if (menuID.hasClass('menu-box-left')) {
          headerAndContent.css("transform", "translateX(" + menuWidth + "px)");
        }

        if (menuID.hasClass('menu-box-right')) {
          headerAndContent.css("transform", "translateX(" + menuWidth * -1 + "px)");
        }
      }

      if (menuEffect === "menu-reveal") {
        if (menuID.hasClass('menu-box-left')) {
          headerAndContent.css("transform", "translateX(" + menuWidth + "px)");
          menuHider.css({
            "transform": "translateX(" + menuWidth + "px)",
            "opacity": "0"
          });
        }

        if (menuID.hasClass('menu-box-right')) {
          headerAndContent.css("transform", "translateX(" + menuWidth * -1 + "px)");
          menuHider.css({
            "transform": "translateX(" + menuWidth * -1 + "px)",
            "opacity": "0"
          });
        }
      }

      menuActivate();
    });
    $(document).click(function () {
      $("#sidebar").removeClass('menu-active');
    }); //profile page banner show more

    $("#profile-banner .fade-content .action-buttons .show-more").click(function (e) {
      e.preventDefault();

      if ($("#profile-banner .fade-content .more-content").hasClass("hide-content")) {
        $(this).html("Show less <i class='fa fa-chevron-up'></i>");
        $("#profile-banner .fade-content .more-content").removeClass('hide-content');
        $("#profile-banner .fade-content .more-content").addClass('show-content');

        if ($("#mobile-layout").is(":visible")) {
          $("#mobile-layout").css("margin-top", "290px");
        }

        $("#profile-banner .fade-content .more-content").slideToggle();
      } else {
        $(this).html("Show more <i class='fa fa-chevron-down'></i>");
        $("#profile-banner .fade-content .more-content").removeClass('show-content');
        $("#profile-banner .fade-content .more-content").addClass('hide-content');
        $("#profile-banner .fade-content .more-content").slideToggle();
        setTimeout(function () {
          if ($("#mobile-layout").is(":visible")) {
            var windowWidth = $(window).width();
            console.log(windowWidth);

            if (windowWidth <= 600) {
              $("#mobile-layout").css("margin-top", "65px");
            } else {
              $("#mobile-layout").css("margin-top", "95px");
            }
          }
        });
      }
    }); //profile page schedule section select fields toogle

    $(".schedule-wrapper .content-wrapper h5 a.select-toogle").click(function (e) {
      e.preventDefault();

      if ($(".schedule-wrapper .content-wrapper .search-bars-wrapper").hasClass("hide-selects")) {
        $(this).html("<i class='fa fa-chevron-up'></i>");
        $(".schedule-wrapper .content-wrapper .search-bars-wrapper").removeClass('hide-selects');
        $(".schedule-wrapper .content-wrapper .search-bars-wrapper").addClass('show-selects');
        $(".schedule-wrapper .content-wrapper .search-bars-wrapper").slideToggle();
      } else {
        $(this).html("<i class='fa fa-chevron-down'></i>");
        $(".schedule-wrapper .content-wrapper .search-bars-wrapper").removeClass('show-selects');
        $(".schedule-wrapper .content-wrapper .search-bars-wrapper").addClass('hide-selects');
        $(".schedule-wrapper .content-wrapper .search-bars-wrapper").slideToggle();
      }
    }); //profile page calendar section select fields toogle

    $(".calendar-wrapper .content-wrapper h5 a.select-toogle").click(function (e) {
      e.preventDefault();

      if ($(".calendar-wrapper .content-wrapper .search-bars-wrapper").hasClass("hide-selects")) {
        $(this).html("<i class='fa fa-chevron-up'></i>");
        $(".calendar-wrapper .content-wrapper .search-bars-wrapper").removeClass('hide-selects');
        $(".calendar-wrapper .content-wrapper .search-bars-wrapper").addClass('show-selects');
        $(".calendar-wrapper .content-wrapper .search-bars-wrapper").slideToggle();
      } else {
        $(this).html("<i class='fa fa-chevron-down'></i>");
        $(".calendar-wrapper .content-wrapper .search-bars-wrapper").removeClass('show-selects');
        $(".calendar-wrapper .content-wrapper .search-bars-wrapper").addClass('hide-selects');
        $(".calendar-wrapper .content-wrapper .search-bars-wrapper").slideToggle();
      }
    }); //theme mode modal

    $(".bd-example-modal-sm .modal-content .switch input").change(function () {
      if (this.checked) {
        $("body").removeClass("theme-dark");
        $("body").addClass("theme-light");
      } else {
        $("body").removeClass("theme-light");
        $("body").addClass("theme-dark");
      }
    }); //profile page follow/unfollow buttons

    $("#profile-banner .profile-img-content .btn-follow").click(function (e) {
      e.preventDefault();
      $(this).addClass("d-none");
      $("#profile-banner .profile-img-content .btn-unfollow").removeClass("d-none");
      $("#profile-banner .profile-img-wrapper .icon-wrapper").removeClass("d-none");
    });
    $("#profile-banner .profile-img-content .btn-unfollow").click(function (e) {
      e.preventDefault();
      $(this).addClass("d-none");
      $("#profile-banner .profile-img-content .btn-follow").removeClass("d-none");
      $("#profile-banner .profile-img-wrapper .icon-wrapper").addClass("d-none");
    }); //magazine home page mobile menu

    $('.mobile-menu-icon').click(function () {
      $(".mobile-menu-wrapper").css("display", "block");
    });
    $('.mobile-close').click(function () {
      $(".mobile-menu-wrapper").css("display", "none");
      $("#sidebar").removeClass('menu-active');
    });
  });
})(jQuery);

/***/ }),

/***/ 1:
/*!**************************************!*\
  !*** multi ./resources/js/custom.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\laragon\www\modelpostmagazine\resources\js\custom.js */"./resources/js/custom.js");


/***/ })

/******/ });